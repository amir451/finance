﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Settings
{
    public interface IApplicationSettings
    {
        string FromEmailAddress { get; }
        string FromEmailDisplayName { get; }
        string SmtpClient { get; }
        string SmtpUserName { get; }
        string SmtpPassword { get; }
        string AwsAccessKey { get; }
        string AwsSecretKey { get; }
        string AwsBucketName { get; }
        string AwsBaseUrl { get; }
        string InstaDbConnectionString { get; }
    }
}
