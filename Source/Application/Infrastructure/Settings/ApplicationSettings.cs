﻿using System.Configuration;

namespace Infrastructure.Settings
{
    public class ApplicationSettings : IApplicationSettings
    {
        public string FromEmailAddress => ConfigurationManager.AppSettings["FromEmailAddress"];
        public string FromEmailDisplayName => ConfigurationManager.AppSettings["FromEmailDisplayName"];
        public string SmtpClient => ConfigurationManager.AppSettings["SmtpClient"];
        public string SmtpUserName => ConfigurationManager.AppSettings["SmtpUserName"];
        public string SmtpPassword => ConfigurationManager.AppSettings["SmtpPassword"];
        public string AwsAccessKey => ConfigurationManager.AppSettings["awsAccessKey"];
        public string AwsSecretKey => ConfigurationManager.AppSettings["awsSecretKey"];
        public string AwsBucketName => ConfigurationManager.AppSettings["bucketName"];
        public string AwsBaseUrl => ConfigurationManager.AppSettings["awsBaseUrl"];
        public string InstaDbConnectionString => ConfigurationManager.AppSettings["InstaDb"];
    }
}
