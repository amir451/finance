﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Content
{
    public sealed class AmazonS3ContentService : IContentService
    {
        private readonly  Infrastructure.Settings.IApplicationSettings _applicationSettings;

        public AmazonS3ContentService(Infrastructure.Settings.IApplicationSettings applicationSettings)
        {
            _applicationSettings = applicationSettings ?? throw new ArgumentNullException("Application settings cannot be null.");
        }

        public string GetContentFullUrl(string contentFolderName, string contentFileName, string contentType)
        {
            if (string.IsNullOrWhiteSpace(contentFileName))
                return string.Empty;

            var filepath = string.Empty;

            filepath = contentFileName.Trim(); ;

            if (!string.IsNullOrWhiteSpace(contentFolderName))
                filepath = string.Join("/", contentFolderName, filepath);

            if (!string.IsNullOrWhiteSpace(contentType))
                filepath = string.Join("/", contentType, filepath);

            return string.Join("/", _applicationSettings.AwsBaseUrl, _applicationSettings.AwsBucketName, filepath);
        }

        public bool UploadContent(Stream stream, string fileName, string contentFolderName, string contentType)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException("Filename cannot ne null or empty.");

            var filepath = string.Empty;

            filepath = fileName.Trim();

            if (!string.IsNullOrWhiteSpace(contentFolderName))
                filepath = string.Join("/", contentFolderName, filepath);

            if (!string.IsNullOrWhiteSpace(contentType))
                filepath = string.Join("/", contentType, filepath);

            var bucketName = string.Join("/", _applicationSettings.AwsBucketName, filepath);

            using (var client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
            {
                try
                {
                    PutObjectRequest uploadFile = new PutObjectRequest
                    {
                        BucketName = _applicationSettings.AwsBucketName,
                        Key = fileName,
                        InputStream = stream,
                        CannedACL = S3CannedACL.PublicRead,
                    };

                    PutObjectResponse response = client.PutObject(uploadFile);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
        }
    }
}
