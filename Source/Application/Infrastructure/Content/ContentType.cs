﻿namespace Infrastructure.Content
{
    public static class ContentType
    {
        public const string ExceptionRequest = "ExceptionRequest";
        public const string Status = "Status";
    }
}
