﻿using System.IO;
namespace Infrastructure.Content
{
    public interface IContentService
    {
        bool UploadContent(Stream stream, string fileName, string folderName, string contentType);
        string GetContentFullUrl(string contentFolderName, string contentFileName, string contentType);
    }
}
