﻿using Infrastructure.Logging;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;

namespace Infrastructure
{
	public class ContainerRegistry : Registry
	{
		public ContainerRegistry()
		{
			Scan(scan =>
			{
				scan.TheCallingAssembly();
				scan.WithDefaultConventions();
			});

			For<ILogger>().Use<SerilogLogger>();
		}
	}
}
