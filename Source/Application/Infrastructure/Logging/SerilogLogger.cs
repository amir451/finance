﻿using Serilog;
using System;
namespace Infrastructure.Logging
{
	public class SerilogLogger : ILogger
	{
		public void Verbose(string message, params object[] data)
		{
			Log.Verbose(messageTemplate: message, propertyValues: data, exception: null);
		}

		public void Debug(string message, params object[] data)
		{
			Log.Debug(messageTemplate: message, propertyValues: data, exception: null);
		}

		public void Info(string message, params object[] data)
		{
			Log.Information(messageTemplate: message, propertyValues: data, exception: null);
		}

		public void Warning(string message, params object[] data)
		{
			Log.Warning(messageTemplate: message, propertyValues: data, exception: null);
		}

		public void Error(string message, Exception exception, params object[] data)
		{
			Log.Error(messageTemplate: message, propertyValues: data, exception: exception);
		}
	}
}
