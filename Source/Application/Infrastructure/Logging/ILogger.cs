﻿using System;

namespace Infrastructure.Logging
{
	public interface ILogger
	{
		void Verbose(string message, params object[] data);
		void Debug(string message, params object[] data);
		void Info(string message, params object[] data);
		void Warning(string message, params object[] data);
		void Error(string message, Exception exception, params object[] data);
	}
}
