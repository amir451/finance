﻿using Serilog;
namespace Infrastructure.Logging
{
	public static class SerilogConfiguration
	{
		public static void UseFile(string location)
		{
			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Verbose()
				.WriteTo.File(location, rollingInterval: RollingInterval.Day)
				.CreateLogger();

			Log.Logger.Information("Application started");
		}
	}
}
