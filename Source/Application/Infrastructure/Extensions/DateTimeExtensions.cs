﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
namespace Infrastructure.Extensions
{
	public static class DateTimeExtensions
	{
		private const string AcceptedDateFormat = "yyyy-MM-dd";
		private const string AcceptedTimeFormat = "HH:mm";

		public static IEnumerable<DateTime> Range(this DateTime startDate, DateTime endDate)
		{
			return Enumerable.Range(0, (endDate - startDate).Days + 1).Select(d => startDate.AddDays(d).Date);
		}

		public static bool IsAValidDate(this string date)
		{
			if (date.Equals(default(DateTime).ToString()))
				return false;

			return DateTime.TryParseExact(date, AcceptedDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime outDate);
		}

		public static bool IsAValidTime(this string time)
		{
			return DateTime.TryParseExact(time, AcceptedTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None,
				out DateTime dateTime);
		}

		public static bool IsValidDateTime(this DateTime dateTime)
		{
			return IsAValidDate(dateTime.Date.ToString()) &&
				   IsAValidTime(string.Join(":", dateTime.Hour, dateTime.Minute, dateTime.Second));
		}

		public static string ToAccetptedDateString(this DateTime dateTime)
		{
			return dateTime.ToString(AcceptedDateFormat);
		}
	}
}
