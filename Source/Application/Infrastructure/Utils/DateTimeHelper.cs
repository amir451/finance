﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Infrastructure.Extensions;
using SwissKnife.Diagnostics.Contracts;

namespace Infrastructure.Utils
{
	public static class DateTimeHelper
	{
		public static DateTime DateAndTimeToDateTime(string date)
		{
			Argument.IsNotNullOrWhitespace(date, nameof(date));

			return DateAndTimeToDateTime(date, "0000");
		}

		public static DateTime DateAndTimeToDateTime(string date, string time)
		{
			Argument.IsNotNullOrWhitespace(date, nameof(date));
			Argument.IsValid(date.IsAValidDate(), "Date should be in (yyyy-MM-dd) format.", nameof(date));
			Argument.IsNotNullOrWhitespace(time, nameof(time));
			Argument.IsValid(time.Length == 4, "PickupTime length should be equal to 4.", nameof(time));

			var parsedTime = string.Join(":", Enumerable.Range(0, time.Length / 2)
				.Select(i => time.Substring(i * 2, 2)).ToArray());

			Argument.IsValid(parsedTime.IsAValidTime(), "Time specified is not valid time instance.", nameof(parsedTime));

			return Convert.ToDateTime(date + " " + parsedTime, CultureInfo.InvariantCulture);
		}

		public static long DateTimeToEpochTime(DateTimeOffset dateTime)
		{
			return Convert.ToInt64(dateTime.Subtract(new DateTimeOffset(1970, 1, 1, 0, 0, 0, new TimeSpan())).TotalMilliseconds);
		}

		public static IEnumerable<DateTime> GetDateRange(string fromDate, string toDate)
		{
			Argument.IsValid(fromDate.IsAValidDate(), "Date should be in (yyyy-MM-dd) format.", nameof(fromDate));
			Argument.IsValid(toDate.IsAValidDate(), "Date should be in (yyyy-MM-dd) format.", nameof(toDate));

			var startDate = DateAndTimeToDateTime(fromDate);
			var endDate = DateAndTimeToDateTime(toDate);

			return GetDateRange(startDate, endDate);
		}

		private static IEnumerable<DateTime> GetDateRange(DateTime startDate, DateTime endDate)
		{
			return startDate.Range(endDate);
		}
	}
}
