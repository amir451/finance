﻿using Model.Account;
using Newtonsoft.Json;
using Repository.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FinanceWebsite.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        IUserRepository repo;
        public AccountController(IUserRepository _repo)
        {
            this.repo = _repo;
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserViewModel user = repo.ValidateUser(model);
                TempData["username"] = user.FullName;
                if (!string.IsNullOrEmpty(user.FullName))
                {
                    model.Emailid = user.EmailID;
                    UserProfileViewModel userprof =new UserProfileViewModel();
                    userprof.UserProfileID = 1;
                    if (userprof.UserProfileID > 0)
                    {
                        string data = JsonConvert.SerializeObject(userprof);
                        Session["CarProviderID"] = null;
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, model.Username, DateTime.Now, DateTime.Now.AddMinutes(30), false, data);
                        string encTicket = FormsAuthentication.Encrypt(ticket);

                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(cookie);                   
                       
                        if (userprof.LkpMemberships != null)
                        {
                            return RedirectToAction("Dashboard", "Home");                         

                        }
                        else
                        {
                            return RedirectToAction("UnAuthorize", "Account");
                        }
                    }
                    else
                    {
                        return RedirectToAction("UnAuthorize", "Account");
                    }

                }
                ViewBag.message = "Sorry ! Login failed";
            }
            return View();
        }

        public ActionResult UnAuthorize()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}