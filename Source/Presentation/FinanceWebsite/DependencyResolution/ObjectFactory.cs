﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinanceWebsite.DependencyResolution
{
    /* This implementation is taken from https://stackoverflow.com/questions/30388081/how-to-resolve-dependency-in-static-class-with-unity
     This class is now implemented to create instances of IContentService types 
     TODO:SSS -> Generalize it so that we can create instance of all interfaces from static class.      
  */
    public static class ObjectFactory<T>
    {
        static Func<T> _instance;

        public static void SetFactory(Func<T> instance)
        {
            _instance = instance;
        }

        public static T GetInstance()
        {
            if (_instance == null) throw new InvalidOperationException("You can not create a context without first building the factory.");

            return _instance();
        }
    }
}