﻿using Microsoft.Practices.Unity;
using Repository.Account;
using System.Web.Mvc;
using Unity.Mvc5;

namespace FinanceWebsite.DependencyResolution
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            //container.RegisterType(typeof(IUserRepository<>), typeof(UserRepository<>));
            container.RegisterType<IUserRepository, UserRepository>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            

        }
    }
}