﻿using Microsoft.Practices.Unity;
using Repository.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinanceWebsite.Security
{
    public class RestictedToUnAssigned : AuthorizeAttribute
    {

        public CustomPrincipal CurrentUser
        {
            get
            {
                return HttpContext.Current.User as CustomPrincipal;
            }
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var container = new UnityContainer();
            container.RegisterType<IUserRepository, UserRepository>();
            var repo = container.Resolve<IUserRepository>();

            base.OnAuthorization(filterContext);
            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            string ActionUrl = "/" + controller + "/" + action;
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                bool checkvalidationaction = true;// repo.CheckAuthorization(CurrentUser.UserProfileID, ActionUrl); //repo.CheckAuthorization(filterContext.HttpContext.User.Identity, ActionUrl);
                if (checkvalidationaction == false)
                {
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "UnAuthorize", area = "" }));
                    return;
                }


            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "Login", area = "" }));
                return;
            }

        }
    }
}