﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FinanceWebsite.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public CustomPrincipal(string username)
        {
            Identity = new GenericIdentity(username);
        }
        public IIdentity Identity
        {
            get;
            private set;
        }
        public bool IsInRole(string role)
        {
            if (!LkpMemberships.Any(r => role.Contains(r)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public int UserProfileID { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Image { get; set; }
        public int ProfileID { get; set; }
        public int CarProviderID { get; set; }
        public int ClientcoID { get; set; }
        public int[] StateID { get; set; }
        public int?[] CityID { get; set; }
        public Boolean checkMylesUser { get; set; }
        public string[] LkpMemberships { get; set; }
        public int DesignationId { get; set; }
        public string UserID { get; set; }
    }
}