﻿using Model.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using FinanceWebsite.Security;
using FinanceWebsite.DependencyResolution;

namespace FinanceWebsite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            UnityConfig.RegisterComponents();
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //UnityConfig.RegisterComponents();
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            ////ModelBinderConfig.RegisterModelBinders();
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                UserProfileViewModel serializeModel = JsonConvert.DeserializeObject<UserProfileViewModel>(authTicket.UserData);

                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserProfileID = serializeModel.UserProfileID;
                newUser.FirstName = serializeModel.FirstName;
                newUser.LastName = serializeModel.LastName;
                newUser.LkpMemberships = serializeModel.LkpMemberships;
                newUser.ProfileID = serializeModel.ProfileID;
             
                newUser.DesignationId = serializeModel.DesignationId;
                newUser.Image = serializeModel.Image;
                newUser.UserID = serializeModel.UserID;
                newUser.ClientcoID = serializeModel.ClientcoID;
                if (serializeModel.StateID.Length > 0)
                    newUser.StateID = serializeModel.StateID;
                else
                    newUser.StateID = null;

                if (serializeModel.CityID.Length > 0)
                    newUser.CityID = serializeModel.CityID;
                else
                    newUser.CityID = null;

                if (newUser.ProfileID == 1)
                {
                    newUser.checkMylesUser = true;
                }
                else
                {
                    newUser.checkMylesUser = false;
                }
                HttpContext.Current.User = newUser;
            }
            else
            {
                
                 if (HttpContext.Current.Request.Url.AbsolutePath != "/Account/Login" && HttpContext.Current.Request.Url.AbsolutePath != "/")
                    Response.Redirect("~/Account/Login?url=" + HttpContext.Current.Request.Url.AbsolutePath);
            }


        }
    }
}
