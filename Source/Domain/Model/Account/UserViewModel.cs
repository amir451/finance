﻿using System;

namespace Model.Account
{
    public class UserViewModel 
    {
        public int SysUserID { get; set; }
        public string LoginID { get; set; }
        public string UserPwd { get; set; }
        public string FName { get; set; }
        public string FullName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string HomeAddress { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EmailID { get; set; }
        public Nullable<int> DesigID { get; set; }
        public Nullable<int> ReportsToID { get; set; }
        public int UnitID { get; set; }
        public Nullable<bool> OutSiderYN { get; set; }
        public string AccessType { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> ChangePass { get; set; }
        public string accessMACIPs { get; set; }
        public Nullable<System.DateTime> PasswordChange { get; set; }
        public string RelationShipType { get; set; }
        public Nullable<bool> ByPassYN { get; set; }
        public Nullable<int> UserTypeID { get; set; }
        public string ClientCoId { get; set; }
    }
}
