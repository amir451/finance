﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Account
{
    public class UserProfileViewModel 
    {
        public int UserProfileID { get; set; }
        public string UserID { get; set; }
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }        
        public int ClientcoID { get; set; }
        public int[] StateID { get; set; }
        public int?[] CityID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Lastlogin { get; set; }
        public DateTime CreatedOn { get; set; }
        public string[] LkpMemberships { get; set; }
        public int DesignationId { get; set; }
        public string todate { get; set; }
        public string fromdate { get; set; }
        public string Image { get; set; }
        public string ValidatedBy { get; set; }

       

        public bool? Active { get; set; }
      
    }
}
