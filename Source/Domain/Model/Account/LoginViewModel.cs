﻿using System.ComponentModel.DataAnnotations;
namespace Model.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        //[Required(ErrorMessage = "Contact Number is required.")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Type only integer number")]
        public string contactno { get; set; }
        public string Emailid { get; set; }
        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }

        public string companyname { get; set; }
    }
}
