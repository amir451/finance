//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class CORVendorMaster
    {
        public int VendorID { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public int OwnershipID { get; set; }
        public string VendorAddress { get; set; }
        public int LocationID { get; set; }
        public string ContactFName { get; set; }
        public string ContactMName { get; set; }
        public string ContactLName { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string AlternateMobileno { get; set; }
        public string EmailID { get; set; }
        public string PANNo { get; set; }
        public Nullable<bool> GSTINYN { get; set; }
        public string GSTIN { get; set; }
        public string IFSCCode { get; set; }
        public string BranchName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAddress { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    }
}
