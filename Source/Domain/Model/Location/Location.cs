﻿using System;
namespace Model.Location
{
   public class Location
    {
        public int LocationID { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<DateTime> ModifyDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    }
}
