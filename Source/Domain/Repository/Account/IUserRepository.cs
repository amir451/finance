﻿using System;
using Model.Account;

namespace Repository.Account
{
    public interface IUserRepository 
    {
        #region For login
        UserViewModel ValidateUser(LoginViewModel model);
        //UserProfileViewModel ValidateUserprofile(string userid, LoginViewModel model);
        #endregion
    }
}
