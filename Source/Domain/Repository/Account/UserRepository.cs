﻿using Model;
using Model.Account;
using System;
using System.Linq;

namespace Repository.Account
{
    public class UserRepository : IUserRepository
    {          
        //public UserRepository(Carzonrent_FinanceEntities carzOnRentDbContext)
        //    : base(carzOnRentDbContext)
        //{
        //}
        private Carzonrent_FinanceEntities CarzonrentDbContext;
      

   
        public UserRepository(Carzonrent_FinanceEntities context)
        {
            this.CarzonrentDbContext = context;
            
        }

        
        #region For UserLogin
        public UserViewModel ValidateUser(LoginViewModel model)
        {
            UserViewModel data = new UserViewModel();
            var obj = CarzonrentDbContext.CORSysUsersMasters.FirstOrDefault(x => x.LoginID == model.Username && x.UserPwd== model.Password);

            if (obj != null && obj.IsActive == true)
            {
                data.SysUserID = obj.SysUserID;
                data.Active = obj.IsActive;
                data.FullName = obj.FName;
                data.EmailID = obj.EmailID;
                #region Insta login history maintain
                
                #endregion
            }
            return data;
        }
        #endregion

        //#region For DataContext Disposing
        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this.disposed)
        //    {
        //        if (disposing)
        //        {
        //            CarzonrentDbContext.Dispose();                    
                   
        //        }
        //    }
        //    this.disposed = true;
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
        //#endregion
    }
}
