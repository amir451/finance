﻿using Model;
using SwissKnife.Diagnostics.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
	public abstract class BaseRepository
	{
		protected BaseRepository(Carzonrent_FinanceEntities carzOnRentDbContext)
		{
			Argument.IsNotNull(carzOnRentDbContext, nameof(carzOnRentDbContext));
			

			CarzonrentDbContext = carzOnRentDbContext;
			
		}

		protected Carzonrent_FinanceEntities CarzonrentDbContext { get; set; }
		
	}
}
