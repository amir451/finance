﻿using SwissKnife;
using Model;

namespace Repository.Location
{
    interface ILocationRepository
    {
        Option<CORLocationMaster> GetLocationMaster();
    }
}
