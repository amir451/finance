﻿using System;
using SwissKnife;
using Model;
using System.Linq;

namespace Repository.Location
{
    public class LocationRepository : BaseRepository
    {
        public LocationRepository(Carzonrent_FinanceEntities carzOnRentDbContext)
          : base(carzOnRentDbContext)
        {
        }
        public Option<CORLocationMaster> GetLocationMaster()
        {
           
            return CarzonrentDbContext.CORLocationMasters.FirstOrDefault();
        }
    }
}
